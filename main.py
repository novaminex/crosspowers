# Create mock NovaBox data using Blueberry flight.
import numpy as np
import matplotlib.pyplot as plt
import array,sys
from struct import pack, unpack
import os

def makeGPSRecord(folder,out):
    #Extract Data from Blueberry records to produce simulated NovaBox GPS Record
    longGPS = np.fromfile(folder+"GPS-lon.dat", dtype=np.float64)
    latGPS = np.fromfile(folder+"GPS-lat.dat", dtype=np.float64)
    temp = np.fromfile(folder+"ConeNFilt.dat", dtype=np.float32)
    # indices = np.arange(1, temp.size, (temp.size / latGPS.size))
    indices = np.floor(np.arange(1, temp.size, (temp.size / latGPS.size)))
    all = np.array([latGPS, longGPS, np.arange(0, latGPS.size), indices]).transpose()
    # Convert sample numbers and time stamps to uint64 format ready for transfer functions codes
    for i in range(0, all.shape[0]):
        # Timestamp column
        b = pack('Q', all[i, 2].astype(np.uint64))
        all[i, 2] = (unpack('d', b)[0])
        # Sample number columns
        b = pack('Q', all[i, 3].astype(np.uint64))
        all[i, 3] = (unpack('d', b)[0])
    all_ = all.flatten()
    f = open(folder+out, "wb")
    all_.tofile(f)

def makeGPS_stationList(folder,out):
    # Make a list of station GPS values
    longGPS = np.fromfile(folder+"GPS-lon.dat", dtype=np.float64)
    latGPS = np.fromfile(folder+"GPS-lat.dat", dtype=np.float64)
    f = open(folder+out, "w")
    for i in range(0, all.shape[0]):
        f.write(str(latGPS[i])+","+str(longGPS[i])+'\n')
    f.close()

def extractSecion(name,out):
    temp = np.fromfile(name, dtype=np.float32)#, count=(50000 * 5))
    f = open(out, "wb")
    temp.tofile(f)

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    #Create GPS binary from Bluberry data
    folder = "C:/Users/johng/Desktop/2016/rd4TF/"
    os.chdir(folder)
    out = "gpsRecord1.bin"
    makeGPSRecord("C:/Users/johng/Desktop/2016/rd4TF/", out)
    # makeGPS_stationList("C:/Users/johng/Desktop/01Jun2019_1405/rd4TF/", "stnLocations.txt")
    extractSecion(folder+"ConeNFilt.dat", "C:/Users/johng/Desktop/c1.bin")
    extractSecion(folder+"ConeWFilt.dat", "C:/Users/johng/Desktop/c2.bin")
    extractSecion(folder+"ConeVFilt.dat", "C:/Users/johng/Desktop/c3.bin")
    extractSecion(folder+"BaseFiltN_.dat", "C:/Users/johng/Desktop/b1.bin")
    extractSecion(folder+"BaseFiltW_.dat", "C:/Users/johng/Desktop/b2.bin")

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
