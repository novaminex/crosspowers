QT -= gui

CONFIG += c++17 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        emproinifile.cpp \
        main.cpp \
        xpower9coil.cpp

TRANSLATIONS += \
    XPowers5Coil_en_CA.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    emproinifile.h \
    xpower9coil.h

INCLUDEPATH += "C:/local/boost_1_87_0/"
INCLUDEPATH += "C:/local/eigen-3.4.0/"
INCLUDEPATH += "C:/local/fftw-3.3.5-dll64"

DEPENDPATH += "C:/local/fftw-3.3.5-dll64"

LIBS += -LC:/local/boost_1_87_0/stage/lib/
LIBS += -LC:/local/fftw-3.3.5-dll64/ -llibfftw3-3

RESOURCES += \
    Resources.qrc
