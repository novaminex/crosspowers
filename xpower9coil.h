#ifndef XPOWER5COIL_H
#define XPOWER5COIL_H
#include <vector>
#include <fstream>
#include <complex>
#include <string>
#include <Eigen/Dense>
#include <algorithm>
#include <exception>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <iostream>
#include <optional>
#include <numeric>
#include <filesystem>

#include "emproinifile.h"

class INIValues{
public:
    int length;
    std::string x,y,z; //mandatory
    std::optional<std::string> xb,yb,zb,xr,yr,zr;
    std::string windowType;
    std::string GPSLog;
    std::string outFileName;
    std::string Timestamps;
    std::string bands;
    std::optional<std::string> sfericsListFile;
    std::optional<std::string> CreateFakeTimestampsAndGPS;
    INIValues():xb(std::nullopt),yb(std::nullopt),zb(std::nullopt),
                xr(std::nullopt),yr(std::nullopt),zr(std::nullopt)
    {}
};

class XPower9coil
{
public:    
    XPower9coil(const std::string iniFileName);
    void runall(std::optional<std::filesystem::path> toINIFILE = std::nullopt);

    class Timestamps{
        ///Store PPS sample info and timing ready for interpolation.
    public:

        //"Timestamps.bin" format:
        struct Timestamp{
            double timestamp;
            double sampleNumber;
            double preceedingSample2;
            double preceedingSample1;
            double followingSample1;
            double followingSample2;
        };

        std::vector<double> getTimeValues(){
            std::vector<double> vals;
            for(auto &x:records){
                vals.push_back(x.timestamp);
            }
            return vals;
        }

        Timestamp getTimestampAtSample(const uint64_t index)
        {   ///Interpolate to get GPS values between recorded points.
            using namespace std;

            //Error checks:
            if(records.empty()){
                throw std::runtime_error("GPS records empty");
            }
            if(records.size()==1){
                return records.front();
            }
            if(!is_sorted(records.begin(),records.end(),[](auto a, auto b) { return (a.sampleNumber < b.sampleNumber);})){
                cout<<"WARNING TIMESTAMPS SAMPLENUMBERS NOT SORTED IN ASCENDING ORDER"<<endl;
                sort(records.begin(),records.end(),[](auto a, auto b) { return (a.sampleNumber < b.sampleNumber);});
            }
            if(!is_sorted(records.begin(),records.end(),[](auto a, auto b) { return (a.timestamp < b.timestamp);})){
                cout<<"WARNING TIMESTAMPS TIME VALUES NOT SORTED IN ASCENDING ORDER"<<endl;
                //                sort(records.begin(),records.end(),[](auto a, auto b) { return (a.timestamp < b.timestamp);});
            }
            auto it = unique(records.begin(),records.end(),[](auto a, auto b) {return (a.sampleNumber == b.sampleNumber);});
            if(static_cast<size_t>(distance(records.begin(),it)) != records.size()){
                throw runtime_error("TIMESTAMPS sample numbers are not unique: ");
            }

            Timestamp interpolated;
            interpolated.sampleNumber=static_cast<double>(index);
            if(index <= records.front().sampleNumber){ //Before or at start of set
                return records.front();
            }
            if(index >= records.back().sampleNumber){ //After or at end of set
                return records.back();
            }

            std::vector<Timestamp> records_=records;

            //It's between these two points in the middle of the set.
            vector<Timestamp>::iterator after = lower_bound(records.begin(),records.end(),index,[](Timestamp a, double b) { return (a.sampleNumber < b);});
            vector<Timestamp>::iterator before = after-1;
            double a=((after->sampleNumber)-index)/((double)((after->sampleNumber)-(before->sampleNumber)));
            double b=(index-(before->sampleNumber))/((double)((after->sampleNumber)-(before->sampleNumber)));
            //            interpolated.sampleNumber = (before->sampleNumber)*a+(after->sampleNumber)*b;
            interpolated.timestamp =((before->timestamp)*a+(after->timestamp)*b);

            return interpolated;
        }

        void loadRecordsFromFile(std::ifstream &openedFile){
            //Load records vector
            if(openedFile.is_open()){
                openedFile.seekg(0, std::ios_base::end);
                size_t len = openedFile.tellg();
                openedFile.seekg(0, std::ios_base::beg);
                //Error Check:
                if(len%8 != 0){
                    throw std::runtime_error("Incorrect length in timestamps file: "+std::to_string(len)+" bytes");
                }

                //If Reading "timestamps.bin"
                for(size_t i=0;i<len;i=i+(6*8)){
                    Timestamp temp;
                    double a,b,c;
                    openedFile.read((char *) &a, sizeof(double));
                    openedFile.read((char *) &b, sizeof(double));
                    //Next four are unused
                    openedFile.read((char *) &c, sizeof(double)); //Preceding sample -1
                    openedFile.read((char *) &c, sizeof(double)); //Preceding sample
                    openedFile.read((char *) &c, sizeof(double)); //Next sample
                    openedFile.read((char *) &c, sizeof(double)); //Nex sample +1
                    temp.timestamp=a;
                    temp.sampleNumber=b;
                    records.push_back(temp);
                }
            }
        }

        void createFakeTimestamps(size_t dataLength, const double samplerate){
            ///For testing purposes.
           records.clear();
            size_t L = std::floorl(dataLength/samplerate);
            for(size_t i=0; i<L; i++){
                Timestamp r;
                r.timestamp=i;
                r.sampleNumber=std::floorl(samplerate*i);
                r.preceedingSample2=-1.0;
                r.preceedingSample1=-0.9; //Pretend it's touching the edge of the pulse
                r.followingSample1=0.9;
                r.followingSample2=1.0;
                records.push_back(r);
            }
        }

        void writeTimestampsToBinary(const std::string outFileName){
            ///Make a binary copy of timestamps

            const std::string outFreqFileName = outFileName+".bin";
            std::ofstream openedFile;
            openedFile.open(outFreqFileName.c_str(),std::ios::out | std::ios::binary);
            if(!(openedFile.is_open())){
                throw std::runtime_error("Couldn't open binary file: "+outFreqFileName);
            }

            if(openedFile.is_open()){
                for(size_t i=0;i<records.size();i++){
                    Timestamp temp =records[i];
                    openedFile.write(reinterpret_cast<char*>(&(temp.timestamp)),sizeof(double));
                    openedFile.write(reinterpret_cast<char*>(&(temp.sampleNumber)),sizeof(double));
                    openedFile.write(reinterpret_cast<char*>(&(temp.preceedingSample2)),sizeof(double));
                    openedFile.write(reinterpret_cast<char*>(&(temp.preceedingSample1)),sizeof(double));
                    openedFile.write(reinterpret_cast<char*>(&(temp.followingSample1)),sizeof(double));
                    openedFile.write(reinterpret_cast<char*>(&(temp.followingSample2)),sizeof(double));
                }
            }
        }

    private:
        std::vector<Timestamp> records;
    };
    class GPS{
        ///Store GPS data and interpolate between samples.
    public:

        //"LatLong.bin" format:
        struct GPSRecord{
            double latitude,longitude;
            uint32_t timestamp; //GPS fix times. Crude timing.
            uint32_t blankSpace; //There's an empty space in the file structure.
        };
        std::vector<uint32_t> getTimestamps(){
            std::vector<uint32_t> temp;
            for(auto &x:records){
                temp.push_back(x.timestamp);
            }
            return temp;
        }
        GPSRecord getGPSatTime(const double timeSec)
        {   ///Interpolate to get GPS values between recorded points.
            using namespace std;

            //Error checks:
            if(records.empty()){
                throw std::runtime_error("GPS records empty");
            }
            if(records.size()==1){
                return records.front();
            }
            if(!is_sorted(records.begin(),records.end(),[](auto a, auto b) { return (a.timestamp < b.timestamp);})){
                cout<<"WARNING GPS TIMESTAMPS NOT SORTED IN ASCENDING ORDER"<<endl;
                sort(records.begin(),records.end(),[](auto a, auto b) { return (a.timestamp < b.timestamp);});
            }
            auto it = unique(records.begin(),records.end(),[](auto a, auto b) {return (a.timestamp == b.timestamp);});
            if(static_cast<size_t>(distance(records.begin(),it)) != records.size()){
                //                throw runtime_error("GPS sample numbers are not unique: ");
                cout<<"WARNING REMOVING REPEAT GPS FIX TIME VALUES FROM GPSLog"<<endl;
                records.resize(distance(records.begin(),it));
            }

            //        std::vector<GPSRecord> tempGPS = records;
            vector<double> tempTimes;

            GPSRecord interpolated;
            interpolated.timestamp=static_cast<uint32_t>(timeSec);
            uint32_t tempGPSRecord =records.front().timestamp;
            uint32_t tempTime =static_cast<uint32_t>(timeSec);
            if(static_cast<uint32_t>(timeSec) <= records.front().timestamp){ //Before or at start of set
                return records.front();
            }
            if(static_cast<uint32_t>(timeSec) >= records.back().timestamp){ //After or at end of set
                return records.back();
            }

            //It's between these two points in the middle of the set.
            vector<GPSRecord>::iterator after = lower_bound(records.begin(),records.end(),timeSec,
                                                            [](GPSRecord a, double b)
            {
                    return (a.timestamp < static_cast<uint32_t>(b));
        });
            vector<GPSRecord>::iterator before = after-1;
            double a=((after->timestamp)-timeSec)/((double)((after->timestamp)-(before->timestamp)));
            double b=(timeSec-(before->timestamp))/((double)((after->timestamp)-(before->timestamp)));
            interpolated.latitude = (before->latitude)*a+(after->latitude)*b;
            interpolated.longitude = (before->longitude)*a+(after->longitude)*b;
            //        interpolated.timestamp =static_cast<uint32_t>(ceill((before->timestamp)*a+(after->timestamp)*b));
            interpolated.timestamp =static_cast<uint32_t>(roundl((before->timestamp)*a+(after->timestamp)*b));
            //tempTimes.push_back(roundl((before->timestamp)*a+(after->timestamp)*b));
            return interpolated;
        }
        void loadRecordsFromFile(std::ifstream &openedFile){
            //Load records vector
            if(openedFile.is_open()){
                openedFile.seekg(0, std::ios_base::end);
                size_t len = openedFile.tellg();
                openedFile.seekg(0, std::ios_base::beg);
                len = len-openedFile.tellg();

                //Error Check:
                if(len%8 != 0){
                    throw std::runtime_error("Incorrect length in gps file: "+std::to_string(len)+" bytes");
                }

                //If Reading "LatLong.bin"
                for(size_t i=0;i<len;i=i+24){
                    GPSRecord temp;
                    double a,b,tval_double = 0.0;
                    uint32_t c,d;
                    openedFile.read((char *) &a, sizeof(double));
                    openedFile.read((char *) &b, sizeof(double));
                    openedFile.read((char *) &tval_double, sizeof(double));
                    // openedFile.read((char *) &d, sizeof(uint32_t)); //Blank space
                    temp.latitude=a;
                    temp.longitude=b;
                    temp.timestamp=(uint32_t)tval_double;
                    records.push_back(temp);
                }
            }
        }
        void createFakeGPS(size_t dataLength, const double samplerate){
            ///For testing purposes.
            records.clear();
            size_t L = std::floorl(dataLength/samplerate);
            for(size_t i=0; i<L; i++){
                GPSRecord r;
                r.latitude=0.0;
                r.longitude=0.0;
                r.timestamp=std::floorl(i*samplerate);
                r.blankSpace=0;
                records.push_back(r);
            }
        }
        void writeRecordsToBinary(const std::string outFileName){
            ///Make a binary copy of the data in the program

            const std::string outFreqFileName = outFileName+".bin";
            //Open binary output file for cross powers, lat, long, UTC time for each station
            std::ofstream openedFile;
            openedFile.open(outFreqFileName.c_str(),std::ios::out | std::ios::binary);
            if(!(openedFile.is_open())){
                throw std::runtime_error("Couldn't open binary file: "+outFreqFileName);
            }

            if(openedFile.is_open()){
                for(size_t i=0;i<records.size();i++){
                    GPSRecord temp =records[i];
                    openedFile.write(reinterpret_cast<char*>(&(temp.latitude)),sizeof(double));
                    openedFile.write(reinterpret_cast<char*>(&(temp.longitude)),sizeof(double));
                    openedFile.write(reinterpret_cast<char*>(&(temp.timestamp)),sizeof(uint32_t));
                    openedFile.write(reinterpret_cast<char*>(&(temp.blankSpace)),sizeof(uint32_t));
                }
            }
        }

    private:
        std::vector<GPSRecord> records;
    };

private:
    XPower9coil();
    std::vector<int> sfericIndices;
    INIValues iniFile;
    INIValues readIniFile(std::string iniFileName);
    std::optional<std::ifstream> openFile(std::optional<std::string> filename, std::optional<std::filesystem::path> toINIFILE = std::nullopt);
    uint64_t checkFileLengthsAreEqual(std::optional<std::ifstream> &ch1, std::optional<std::ifstream> &ch2, std::optional<std::ifstream> &ch3,
                                      std::optional<std::ifstream> &ch4, std::optional<std::ifstream> &ch5, std::optional<std::ifstream> &ch6,
                                      std::optional<std::ifstream> &ch7, std::optional<std::ifstream> &ch8, std::optional<std::ifstream> &ch9);
    std::vector<uint64_t> getSfericList(const size_t tsSampleLength, const size_t windowLength, const std::optional<std::string> indicesFile, std::optional<std::filesystem::path> toINIFILE = std::nullopt);
    std::vector<double> getWindowShape(const size_t n, const std::string iniFileWindowName);
    void loadWorkspace(std::optional<std::ifstream> &ch1, std::optional<std::ifstream> &ch2, std::optional<std::ifstream> &ch3,std::optional<std::ifstream> &ch4,
                       std::optional<std::ifstream> &ch5, std::optional<std::ifstream> &ch6,std::optional<std::ifstream> &ch7,std::optional<std::ifstream> &ch8,
                       std::optional<std::ifstream> &ch9,const size_t index,std::vector< std::vector<double> > &workspace);
    void fftWindows(const std::vector<double> &tsWindow, std::vector<std::vector<double> > &workspace, Eigen::MatrixXcd &complexWorkspace);
    // std::vector< std::pair< size_t,size_t> > getFrequencyBands(const std::string node);
    void sumCrossPowers(std::vector<std::complex<double> > &allTables, std::pair< size_t,size_t> &band, Eigen::MatrixXcd &complexWorkspace,const Eigen::MatrixXcd &sumCrossPowers);
    void applyQwindow(Eigen::MatrixXcd &complexWorkspace, const size_t numberOfRows);
    //
    GPS getGPSRecords(const std::string nodeName, std::optional<std::filesystem::path> toINIFILE = std::nullopt);
    Timestamps getTimestampRecords(std::optional<std::string> filename, std::optional<std::filesystem::path> toINIFILE = std::nullopt);
    double calculateShiftUTC(GPS gps,Timestamps timestamps);
    //
    void r2r_FFT(std::vector<double> &coilData,std::vector<double> &copy,const std::vector<double> &tsWindow);
    void swtichToEigen(std::vector<std::vector<double> > &workspace, Eigen::MatrixXcd &complexWorkspace);
    //
    void writeToBinaries(const std::vector<std::complex<double> > &allTables, const size_t tableSize, const std::string outFileName, const std::vector<std::pair<size_t, size_t> > &bands);
    //Experiments  with sferic approach angle:
    void TDCrossPowersWindows(std::vector<std::complex< double > > &TDTable, const std::vector<std::vector<double>> &workspace);
    void writeToBinaries(const std::vector<double> &allTables, const std::string &outFileName);
};

#endif // XPOWER5COIL_H
