#include <QCoreApplication>
#include <QFile>
#include <QDir>
#include <iostream>
#include "xpower9coil.h"
#include <utility>
#include <string>
#include <fftw3.h>
#include <boost/program_options.hpp>
namespace po=boost::program_options;
namespace fs=std::filesystem;

int main(int argc, char *argv[])
{
    using namespace std;
    //Use boost here to select ini file and hand to class constructor.
    try {
        po::options_description desc{"Options"};
        desc.add_options()
                ("help,h", "Help screen")
                ("iniFileName", po::value<string>()->default_value("C:/Users/johng/Desktop/NVBX22_06_16_16_04_51/INIFILE.ini"), "INI file full path");
        po::variables_map vm;
        store(parse_command_line(argc, argv, desc), vm);
        notify(vm);

        if (vm.count("help")){
            ///Move AFMAG.ini into folder as a guide to useage
            QFile::copy(":/MyFiles/INIFILE.ini",QString::fromStdString(fs::current_path().generic_string()+"/AFMAG.ini"));

            //Write to screen.
            cout << desc << '\n';
            cout << "" << '\n';
            cout << "Sample INIFILE format:" << '\n';
            cout << "" << '\n';
            cout << "#Cross Powers Section:" << '\n';
            cout << "[XPowers9Coil]" << '\n';
            cout << "x = C:/Users/johng/Desktop/NVBX22_06_16_16_04_51/RD4TF/channel1Filt.bin" << '\n';
            cout << "y = C:/Users/johng/Desktop/NVBX22_06_16_16_04_51/RD4TF/channel2Filt.bin" << '\n';
            cout << "z = C:/Users/johng/Desktop/NVBX22_06_16_16_04_51/RD4TF/channel3Filt.bin" << '\n';
            cout << "#xb = C:/NVBX22_06_16_16_10_33/RD4TF/channel1Filt.bin" << '\n';
            cout << "#yb = C:/NVBX22_06_16_16_10_33/RD4TF/channel2Filt.bin" << '\n';
            cout << "#zb = C:/NVBX22_06_16_16_10_33/RD4TF/channel3Filt.bin" << '\n';
            cout << "#xr = C:/NVBX22_06_13_17_13_15/RD4TF/channel1Filt.bin" << '\n';
            cout << "#yr = C:/NVBX22_06_13_17_13_15/RD4TF/channel2Filt.bin" << '\n';
            cout << "#zr = C:/NVBX22_06_13_17_13_15/RD4TF/channel3Filt.bin" << '\n';
            cout << "sfericsListFile  = C:/Users/johng/Desktop/01Jun2019_1405/rd4TF/sfericIndices.bin" << '\n';
            cout << "length = 50000" << '\n';
            cout << "windowType = harris" << '\n';
            cout << "sampleRate = 50000" << '\n';
            cout << "Timestamps = C:/Users/johng/Desktop/NVBX22_06_16_16_04_51/timestamps.bin" << '\n';
            cout << "GPSLog = C:/Users/johng/Desktop/NVBX22_06_16_16_04_51/LatLong.bin" << '\n';
            cout << "fileName = C:/Users/johng/Desktop/NVBX22_06_16_16_04_51/RD4TF/crosspowers.bin" << '\n';
            cout << "bands = 25,38,50,60,60,70,25,55,70,300,4500,7000" << '\n';
            cout << "---------------------------------------"<< '\n';
            cout << "          SPECIAL OPTIONS:"<< '\n';
            cout << "---------------------------------------"<< '\n';
            cout << "CreateFakeTimestampsAndGPS = 1 (optional) Overrides normal functioning of" <<endl;
            cout << "program to create a fake Timestamps and GPS file consistent with the length   << '\n'";
            cout << "of the data series   << '\n'";
        }
        else if (vm.count("iniFileName")){
            cout << "-------------------------------------------------- " << '\n';
            cout << "----    CROSS POWERS CALCULATION ROUTINE   ------- " << '\n';
            cout << "-------------------------------------------------- " << '\n'<< '\n';
            cout << " " << '\n';
            cout << "INI file: " << vm["iniFileName"].as<string>() << '\n';
            //Load ini and process:
            XPower9coil process(vm["iniFileName"].as<string>());

            //Use paths relative to INIFILE
            std::filesystem::path f(vm["iniFileName"].as<string>());
            if (!exists(f)){
                throw std::runtime_error("Cannot find ini file: "+vm["iniFileName"].as<string>());
            }
            std::filesystem::path root = f;
            process.runall(root);
        }
    } catch (exception& e)
    {
        cerr << "exception caught: " << e.what() << '\n';
    }
    return 0;
}
