#include "emproinifile.h"
#include <fstream>
#include <sstream>
#include <iomanip>
#include <filesystem>

void EmProIniFile::removeQuotes(std::string &s){
    s.erase(remove(s.begin(),s.end(),'\"'),s.end());
}

std::string EmProIniFile::removeWhitespace(const std::string& str)
{
    std::string str_2 = str;
    str_2.erase(remove_if(str_2.begin(), str_2.end(), isspace), str_2.end());
    return str_2;
}
std::string EmProIniFile::stringToLower(const std::string& s)
{
    std::string rv=s;
    for(size_t i=0; i<rv.size(); i++)
    {
        rv[i] = tolower(rv[i]);
    }
    return rv;
}
void EmProIniFile::removeLeadingAndTrailingWhitespace( std::string& s)
{
    if(s.length()==0)return;
    size_t start=s.find_first_not_of(" \n\r\t");
    size_t end=s.find_last_not_of(" \n\r\t");
    s=(s.substr(start, end-start+1));
}

void EmProIniFile::local_stringChopper(const std::string& str, const std::string& delim, std::vector<std::string>& parts) {
  size_t start, end = 0;
  while (end < str.size()) {
    start = end;
    while (start < str.size() && (delim.find(str[start]) != std::string::npos)) {
      start++;  // skip initial whitespace
    }
    end = start;
    while (end < str.size() && (delim.find(str[end]) == std::string::npos)) {
      end++; // skip to end of word
    }
    if (end-start != 0) {  // just ignore zero-length strings.
      parts.push_back(std::string(str, start, end-start));
    }
  }
  for(size_t i=0; i<parts.size(); i++){
  removeLeadingAndTrailingWhitespace(parts[i]);
  }
}

std::string EmProIniFile::extractSectionName(const std::string s){
    size_t leftBracketPos=s.find_first_of("[");
    if(leftBracketPos==std::string::npos)return"";
    size_t rightBracketPos=s.find_first_of("]");
    if(rightBracketPos==std::string::npos)return"";
    int nchars=rightBracketPos-leftBracketPos-1;
    if(nchars<1)return"";
    return (s.substr(leftBracketPos+1,nchars));
}
void removeOuterQuotes(std::string &s){
    if((s[0]=='\"')&&(s[s.length()-1]=='\"')){
        s=s.substr(1,s.length()-2);
    }
}

std::optional<std::pair<std::string,std::string>> getNameValuePair(const std::string s){
   size_t equalPos=s.find_first_of("=");
   if(equalPos==std::string::npos)return std::nullopt;
   size_t n=equalPos;
   std::string name=s.substr(0,n);
   n=s.length()-equalPos;
   std::string value=s.substr(equalPos+1,n);
   removeOuterQuotes(value);//just in case string wiht spaces is delimited in quotes
   return std::make_pair(name,value);
}

bool EmProIniFile::sectionExists(const std::string sectionName){
    for(size_t i=0; i<m_inifileCache.size(); i++){
        if(m_inifileCache[i].sectionName==sectionName)return true;
    }
    return  false;
}

void EmProIniFile::addToIniCache(const std::string &textLine, const std::string &sectionName, const std::string &variableName, const std::string &value ){
   iniFileLineRecord r;
   r.lineOfText=textLine;
   r.sectionName=(sectionName);
   r.variableName=(variableName);
   r.value=value;

   m_inifileCache.push_back(r);
}

EmProIniFile::EmProIniFile(std::string filePath)
{
    m_filePath=filePath;

    std::ifstream iniFileStream(filePath);

    std::string line;
    std::string lastSectionName="";
    while(getline(iniFileStream, line)) {

        removeLeadingAndTrailingWhitespace(line);

        std::string sectionName=extractSectionName(line);

        if(sectionName.length()!=0){
            //found a new section
            lastSectionName=sectionName;
            addToIniCache(line,sectionName,"","");
            continue;
        }

        //not a section name entry... possibly name value pair associated with last section
        //try to extract a name value pair

        auto optnvp=getNameValuePair(line);
        if(optnvp){//register it
            addToIniCache(line,lastSectionName,(*optnvp).first,(*optnvp).second);
        }
        else{
            //store the comment line eg not useful line
            addToIniCache(line,"","","");
        }
    }
}

std::optional<std::string> EmProIniFile::getVariableOptional(const std::string &Section,const std::string &Variable){

  for(size_t i=0; i<m_inifileCache.size(); i++){
    if(m_inifileCache[i].sectionName==(Section))  {
        if(m_inifileCache[i].variableName==(Variable)){
           return m_inifileCache[i].value;
        }
    }

 }
  return std::nullopt;
}

std::vector<double> EmProIniFile::getVectorDouble(const std::string &Section,const std::string &Variable){
    if(auto opt=getVariableOptional(Section,Variable)){
      std::string outString=opt.value();
      std::vector<std::string>tokens;
      local_stringChopper(outString, ",", tokens);
      std::vector<double> rv(tokens.size());
      for(size_t i=0; i< rv.size(); i++){
        rv[i]=std::stod(tokens[i]);
      }
      return(rv);
    }
    m_errorStrings.push_back("Error:  could not find any " + Variable +" in ini file section "+Section);
    return {};
}

std::optional<double> EmProIniFile::getDouble(const std::string &Section,const std::string &Variable){
    if(auto opt=getVariableOptional(Section,Variable)){
      std::string outString=opt.value();
      double rv=stod(outString);
      return rv;
    }
    m_errorStrings.push_back("Error:  could not find any " + Variable +" in ini file section "+Section);
    return std::nullopt;
}

std::vector<int> EmProIniFile::getVectorInt(const std::string &Section,const std::string &Variable){

    if(auto opt=getVariableOptional(Section,Variable)){
      std::string outString=opt.value();
      std::vector<std::string>tokens;
      local_stringChopper(outString, ",", tokens);
      std::vector<int> rv(tokens.size());
      for(size_t i=0; i< rv.size(); i++){
        rv[i]=std::stoi(tokens[i]);
      }
      return(rv);
    }
   m_errorStrings.push_back("Error:  could not find any " + Variable +" in ini file section "+Section);
    return {};
}

std::vector<bool> EmProIniFile::getVectorBool(const std::string &Section,const std::string &Variable){
    if(auto opt=getVariableOptional(Section,Variable)){
      std::string outString=opt.value();
      std::vector<std::string>tokens;
      local_stringChopper(outString, ",", tokens);

      std::vector<bool> rv(tokens.size());
      for(size_t i=0; i< rv.size(); i++){
        tokens[i]=(tokens[i]);
        if(tokens[i]=="1"||tokens[i]=="true")rv[i]=true;
        else if(tokens[i]=="0"||tokens[i]=="false")rv[i]=false;
        else{
            m_errorStrings.push_back("Error:  "+std::to_string(i)+"th element of bool array input not recognized.  Should be one of (1,0,true,false). While scanning variable " + Variable +" in ini file section "+Section);
             return {};
        }

      }
    return rv;
}
    return {};
}

std::optional<int> EmProIniFile::getInt(const std::string &Section,const std::string &Variable){
    if(auto opt=getVariableOptional(Section,Variable)){
      std::string outString=opt.value();
      double rv=stod(outString);
      return rv;
    }
    m_errorStrings.push_back("Error:  could not find any " + Variable +" in ini file section "+Section);
    return std::nullopt;
}

std::optional<bool> EmProIniFile::getBool(const std::string &Section,const std::string &Variable){

    if(auto opt=getVariableOptional(Section,Variable)){
      std::string s=opt.value();
      removeWhitespace(s);
      if(s=="true")return true;
      if(s=="false")return false;
      if(s=="1")return true;
      if(s=="0")return false;

      m_errorStrings.push_back("Error:  could not find valid value for " + Variable +" in ini file section "+Section+"    Found: "+s+"  Required one of: true,false,1,0");
      return std::nullopt;

    }
    m_errorStrings.push_back("Error:  could not find any " + Variable +" in ini file section "+Section);
    return std::nullopt;
}

std::vector<std::string> EmProIniFile::getVectorString(const std::string &Section,const std::string &Variable){

    if(auto opt=getVariableOptional(Section,Variable)){
      std::string outString=opt.value();
      std::vector<std::string>tokens;
      local_stringChopper(outString, ",", tokens);
      for(size_t i=0; i<tokens.size(); i++)removeQuotes(tokens[i]);
      return(tokens);
    }
    m_errorStrings.push_back("Error:  could not find any " + Variable +" in ini file section "+Section);
    return {};
}

std::optional<std::string> EmProIniFile::getString(const std::string &Section,const std::string &Variable){

    if(auto opt=getVariableOptional(Section,Variable)){
      std::string outString=opt.value();
      std::string rv=outString;
      removeQuotes(rv);
      return rv;
    }
        m_errorStrings.push_back("Error:  could not find any " + Variable +" in ini file section "+Section);
    return std::nullopt;
}
std::string replaceValue(const std::string s, const std::string &valueToSubstitute){
    //assumes value lives after the "=" in string s
    std::string valToSub=valueToSubstitute;
    size_t spacePos=valToSub.find_first_of(" ");
    if(spacePos!=std::string::npos){//contains a space put in quotes
       valToSub="\"" + valToSub +"\"" ;
    }

    std::string rv=s;
    size_t equalPos=s.find_first_of("=");
    if(equalPos==std::string::npos)return(s);//leave unchanged

    size_t firstOfValue=s.find_first_not_of(" \t",equalPos+1);
    if(firstOfValue==std::string::npos)return(s);//leave unchanged
    size_t lastOfValue;
    if(s[firstOfValue]=='\"'){//if its the start of a quote then find the bracketing quote
       lastOfValue=s.find_first_of("\"",firstOfValue);
       if(lastOfValue==std::string::npos){//no bracketing quote found ut one in
           rv=s+"\"";
           lastOfValue=rv.length()-1;
        }
    }
    else{
      lastOfValue=s.find_first_of(" \t",firstOfValue);
      if(lastOfValue==std::string::npos)lastOfValue=rv.length()-1;
      else lastOfValue--;
    }
    rv.replace(firstOfValue,lastOfValue,valueToSubstitute);
    return rv;
}

void EmProIniFile::putString(const std::string &Section,const std::string &Variable, const std::string &s){
   for(size_t i=0; i<m_inifileCache.size(); i++){
       if(m_inifileCache[i].sectionName==(Section)){
           if(m_inifileCache[i].variableName==(Variable)){
               m_inifileCache[i].lineOfText=replaceValue(m_inifileCache[i].lineOfText,s);
           }
       }
   }
}

std::optional<std::string> EmProIniFile::getString(const std::string &Section,const std::string &Variable,const std::string &defaultValueIfMissing){
    if(auto opt=getVariableOptional(Section,Variable)){
        std::string outString=opt.value();
        std::string rv=outString;
        removeQuotes(rv);
        return rv;
    }
    else{
        putString(Section,Variable,defaultValueIfMissing);
       m_errorStrings.push_back("Error:  missing field Section="+Section+" Variable="+Variable+"  Populating new entry with default value="+defaultValueIfMissing);
        return(defaultValueIfMissing);
    }
}

void EmProIniFile::putDouble(const std::string &Section,const std::string &Variable, double doubleToPut, int ndecimals){
    std::stringstream ss;
    ss << std::setprecision(ndecimals)<<std::fixed;
    ss<<doubleToPut;
    putString(Section,Variable,ss.str());
}

void EmProIniFile::putInt(const std::string &Section,const std::string &Variable, int intToPut){
    std::stringstream ss;
    ss<<intToPut;
    putString(Section,Variable,ss.str());
}

void EmProIniFile::putBool(const std::string &Section,const std::string &Variable, bool boolToPut){
    std::stringstream ss;
    ss<<boolToPut;
    putString(Section,Variable,ss.str());
}

void EmProIniFile::putVectorDouble(const std::string &Section,const std::string &Variable,const  std::vector<double> &doubleToPut, int ndecimals){
    std::stringstream ss;
    ss << std::setprecision(ndecimals)<<std::fixed;
    for(size_t i=0; i<doubleToPut.size(); i++){
        ss<<doubleToPut[i];
        if(i<doubleToPut.size()-1)ss<<",";
    }
    putString(Section,Variable,ss.str());
}

void EmProIniFile::putVectorInt(const std::string &Section,const std::string &Variable,const  std::vector<int> &intToPut){
    std::stringstream ss;
    for(size_t i=0; i<intToPut.size(); i++){
        ss<<intToPut[i];
        if(i<intToPut.size()-1)ss<<",";
    }
    putString(Section,Variable,ss.str());
}

void EmProIniFile::putVectorString(const std::string &Section,const std::string &Variable,const  std::vector<std::string> &stringToPut){
    std::stringstream ss;
    for(size_t i=0; i<stringToPut.size(); i++){
        ss<<stringToPut[i];
        if(i<stringToPut.size()-1)ss<<",";
    }
    putString(Section,Variable,ss.str());
}

bool EmProIniFile::entryExistsWordy(const std::string &Section,const std::string &Variable){

    for(size_t i=0; i<m_inifileCache.size(); i++){
      if(m_inifileCache[i].sectionName==(Section))  {
          if(m_inifileCache[i].variableName==(Variable)){
             return true;
          }
      }

   }
 m_errorStrings.push_back("Error: Missing ini file variable:"+Variable+" in Section: "+Section);
 return false;
}
bool EmProIniFile::entryExists(const std::string &Section,const std::string &Variable){


    for(size_t i=0; i<m_inifileCache.size(); i++){
      if(m_inifileCache[i].sectionName==(Section))  {
          if(m_inifileCache[i].variableName==(Variable)){
             return true;
          }
      }

   }
 return false;
}
void EmProIniFile::saveToFileEntriesOnly(std::string path){
    std::ofstream outfile(path);
    if(!std::filesystem::exists(path))return;
    for(size_t i=0; i<m_inifileCache.size(); i++){
        if((m_inifileCache[i].variableName.length()==0)&&(m_inifileCache[i].sectionName.length()>0)) outfile<<"["<<m_inifileCache[i].sectionName<<"]"<<std::endl;
        if(m_inifileCache[i].variableName.length()>0) outfile<<m_inifileCache[i].variableName<<"="<<m_inifileCache[i].value<<std::endl;
    }
}

void EmProIniFile::saveToFile(std::string path){
    std::ofstream outfile(path);
    if(!std::filesystem::exists(path))return;
    for(size_t i=0; i<m_inifileCache.size(); i++){
      outfile<<m_inifileCache[i].lineOfText<<std::endl;
    }
}

void EmProIniFile::saveFile(){
    saveToFile(m_filePath);
}


std::string EmProIniFile::getLastErrorString(){
    if(m_errorStrings.size()==0)return "";
    return m_errorStrings[m_errorStrings.size()-1];
}

std::vector<std::string> EmProIniFile::getErrorStrings(){
  return  m_errorStrings;
}
