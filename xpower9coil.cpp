#include "xpower9coil.h"
#include <filesystem>
#include <utility>
#include <fftw3.h>
#include <sstream>
#include <numeric> // For std::inner_product
using namespace std;
using namespace Eigen;

namespace  { //Local worker functions
size_t calculateNyquistBin(size_t n){
    size_t nyq;
    if(n%2==0){//even
        //cout<<"Even length window "<<n<<" nyquist: ";
        nyq = (n/2)+1;
    }
    else{//odd
        //cout<<"Odd length window "<<n<<" nyquist: ";
        nyq = (n+1)/2;
    }
    //cout<<nyq<<endl;
    return nyq;
}

void loadIntoVector(optional<ifstream> &ch, const size_t index, const size_t length, std::vector<double> &workspace){
    if(!ch->good()) {
        throw runtime_error("Error loadWorkspace: ch1");
    }
    ch->seekg(index*sizeof(float), ios_base::beg);
    for(size_t i=0; i<length; i++){
        float val;
        ch->read((char *) &val, sizeof(float));
        workspace[i]=(double)val;
    }
}

optional<int64_t> channelLength(optional<ifstream> &ch1){
    if(ch1){
        ch1->seekg(0, ios_base::end);
        int64_t length = ch1->tellg();
        ch1->seekg(0, ios_base::beg);
        return length;
    }
    else return nullopt;
}
}

std::optional<std::vector<std::pair<size_t, size_t>>> getFrequencyBands(const std::string& bandsTXT) {
    std::vector<std::pair<size_t, size_t>> result;
    std::vector<size_t> numbers;
    std::stringstream ss(bandsTXT);
    std::string item;

    // Parse the input string and extract numbers
    while (std::getline(ss, item, ',')) {
        try {
            size_t number = std::stoul(item);
            if (number <= 0) {
                return std::nullopt; // Return nullopt if any number is zero or negative
            }
            numbers.push_back(number);
        } catch (const std::invalid_argument& e) {
            return std::nullopt; // Return nullopt if conversion fails
        } catch (const std::out_of_range& e) {
            return std::nullopt; // Return nullopt if number is out of range
        }
    }

    // Check if the number of elements is odd
    if (numbers.size() % 2 != 0) {
        return std::nullopt;
    }

    // Pair the numbers together
    for (size_t i = 0; i + 1 < numbers.size(); i += 2) {
        if (numbers[i] < numbers[i + 1]) {
            result.emplace_back(numbers[i], numbers[i + 1]);
        } else {
            return std::nullopt; // Return nullopt if left value is not smaller than right
        }
    }

    return result;
}

std::optional<ifstream> XPower9coil::openFile(std::optional<std::string> filename, std::optional<std::filesystem::path> toINIFILE)
{
    if(filename){
        ifstream mySource;
        filename->erase(remove(filename->begin(),filename->end(),'\"'),filename->end()); //remove quotes.
        if(toINIFILE){
            filename = toINIFILE->parent_path().generic_string()+"/"+(*filename);
        }
        cout<<"Opening: "<<*filename<<endl;
        mySource.open(filename->c_str(),ios::in | ios_base::binary);
        if(!(mySource.is_open())){
            throw runtime_error("Couldn't open "+*filename);
        }
        return mySource;
    }
    else{
        return nullopt;
    }
}

uint64_t XPower9coil::checkFileLengthsAreEqual(optional<ifstream> &ch1, optional<ifstream> &ch2, optional<ifstream> &ch3,
                                               optional<ifstream> &ch4, optional<ifstream> &ch5, optional<ifstream> &ch6,
                                               optional<ifstream> &ch7, optional<ifstream> &ch8, optional<ifstream> &ch9)
{
    //Calculate common size.
    optional<int64_t> fsize1 = channelLength(ch1);
    optional<int64_t> fsize2 = channelLength(ch2);
    optional<int64_t> fsize3 = channelLength(ch3);
    optional<int64_t> fsize4 = channelLength(ch4);
    optional<int64_t> fsize5 = channelLength(ch5);
    optional<int64_t> fsize6 = channelLength(ch6);
    optional<int64_t> fsize7 = channelLength(ch7);
    optional<int64_t> fsize8 = channelLength(ch8);
    optional<int64_t> fsize9 = channelLength(ch9);

    //Error Check:
    vector< optional<int64_t> > FS = {fsize1,fsize2,fsize3,fsize4,fsize5,fsize6,fsize7,fsize8,fsize9};
    auto it = partition(FS.begin(),FS.end(),[](optional<int64_t> a){return a.has_value();}); //The valid channels
    auto it2 = unique(FS.begin(),it,[](auto a,auto b){return *a == *b;}); //Same length
    if(it2-FS.begin() != 1){
        throw runtime_error("File sizes must all be equal");
    }
    if(**it2 == -1){
        throw runtime_error("input file empty");
    }
    if(**it2 < 0){
        throw runtime_error("malfunction reading file size: "+to_string(**it2)+" bytes");
    }
    if(**it2 % 4 != 0){
        throw runtime_error("data channel wrong size to be filled with float values: "+to_string(**it2)+" bytes");
    }

    cout<<"Total samples: "<<((**it2)/4)<<endl;//Floats have 4 bytes
    return ((**it2)/4);
}

std::vector<double> XPower9coil::getWindowShape(const size_t n, const std::string windowName)
{
    ///Create a window of correct size following name from INI file

    //Access ini file:
    vector<double> windowShape;
    // if(windowName){
    cout<<"Window Shape: "<<windowName<<endl;
    if(windowName.compare(string("harris"))==0){ //Only option so far is harris.
        windowShape.assign(n,0.0);
        for(size_t i = 0;i<windowShape.size();i++){
            windowShape[i] = 0.35875 - 0.48829*cos((2*3.14*i)/(n-1))+0.14128*cos((4*3.14*i)/(n-1))-0.01168*cos((6*3.14*i)/(n-1));
        }
    }
    else{
        if(windowName.compare(string("ones"))==0){ //another default
            windowShape.assign(n,0.0);
            for(size_t i = 0;i<windowShape.size();i++){
                windowShape[i] = 1.0;//
            }
        }
        else{
            throw runtime_error("Incorrect window option: "+windowName);
        }
    }
    return windowShape;
}

std::string truncate_bin_extension(const std::string input) {
    if (input.size() >= 4 && input.substr(input.size() - 4) == ".bin") {
        return input.substr(0, input.size() - 4);
    }
    return input;
}

void XPower9coil::runall(std::optional<std::filesystem::path> toINIFILE)
{
    ///Make cross-powers according to parameters in ini file and fill a series of tables covering every ground station
    /// on the survey line, together with lat,long, and utc time for each station. Save them as binary files, one file per
    /// frequency band.

    optional<ifstream> ch1 = openFile(iniFile.x,toINIFILE);
    optional<ifstream> ch2 = openFile(iniFile.y,toINIFILE);
    optional<ifstream> ch3 = openFile(iniFile.z,toINIFILE);
    optional<ifstream> ch4 = openFile(iniFile.xb,toINIFILE);
    optional<ifstream> ch5 = openFile(iniFile.yb,toINIFILE);
    optional<ifstream> ch6 = openFile(iniFile.zb,toINIFILE);
    optional<ifstream> ch7 = openFile(iniFile.xr,toINIFILE);
    optional<ifstream> ch8 = openFile(iniFile.yr,toINIFILE);
    optional<ifstream> ch9 = openFile(iniFile.zr,toINIFILE);

    const uint64_t dataLength = checkFileLengthsAreEqual(ch1,ch2,ch3,ch4,ch5,ch6,ch7,ch8,ch9);

    //Open binary output file for cross powers, lat, long, UTC time for each station
    string outFileName=iniFile.outFileName;
    namespace fs=std::filesystem;
    outFileName.erase(remove(outFileName.begin(),outFileName.end(),'\"'),outFileName.end()); //remove quotes.
    outFileName = truncate_bin_extension(outFileName);
    //Allow for relative path from INIFILE
    if(toINIFILE){
        outFileName = toINIFILE->parent_path().generic_string()+"/crosspowers/"+(outFileName);
    }
    //Look for folder for crosspowers
    if(!fs::exists(fs::path(outFileName).parent_path())){
        bool worked = std::filesystem::create_directory(fs::path(outFileName).parent_path().generic_string());
        if(!worked){
            throw runtime_error("Failed trying to create folder: "+fs::path(outFileName).parent_path().generic_string());
        }
        cout<<"Created folder: "<<fs::path(outFileName).parent_path().generic_string()<<endl;
    }

    ///Produce cross powers for stations along flight path  tagged with GPS location and timing.
    //Load in GPS record containing GPS info: lat, long, time records.
    GPS gps = getGPSRecords(iniFile.GPSLog, toINIFILE);

    //Load in record containing crude GPS times, PPS pulse sample numbers, and PPS signal values before and after pulse detection.
    Timestamps timestamps = getTimestampRecords(iniFile.Timestamps, toINIFILE);

    //Determine UTC timeshift
    double UTC_Offset = calculateShiftUTC(gps,timestamps);

    //allocate workspace for loading data and performing FFTs;
    vector< std::vector<double> > workspace;
    // const boost::optional<size_t> n=iniFile.get_optional<size_t>("XPowers9Coil.length");
    size_t n=iniFile.length;
    cout<<" window length: "<<n<<endl;
    workspace.push_back(vector<double>(n)); //coil 1
    workspace.push_back(vector<double>(n)); //coil 2
    workspace.push_back(vector<double>(n)); //coil 3
    workspace.push_back(vector<double>(n)); //coil 4
    workspace.push_back(vector<double>(n)); //coil 5
    workspace.push_back(vector<double>(n)); //coil 6
    workspace.push_back(vector<double>(n)); //coil 7
    workspace.push_back(vector<double>(n)); //coil 8
    workspace.push_back(vector<double>(n)); //coil 9
    workspace.push_back(vector<double>(n)); //Last vector is used for copying data during fft routine.

    const vector<double> window = getWindowShape(n,iniFile.windowType);
    vector<uint64_t> indices = getSfericList(dataLength,window.size(),iniFile.sfericsListFile, toINIFILE);
    const size_t nyq = calculateNyquistBin(n);

    const optional<vector<pair<size_t,size_t> > > bands = getFrequencyBands(iniFile.bands); //This is in terms of bin numbers.

    //Error check
    //    cout<<"Series length "<<to_string(in.size())<<endl;
    //    const size_t N=determine_FFT_length(in.size());
    for(auto &x:(*bands)){
        if(x.first<0){
            throw runtime_error("Check bands. Bin "+std::to_string(x.first)+" is below zero ");
            //            x.first=0;
        }
        if(nyq<x.second){
            throw runtime_error("Check bands. Bin "+std::to_string(x.second)+" is above nyquist");
            //            x.second=nyq-1;
        }
        if(x.second<=x.first){
            throw runtime_error("Check bands. Bin "+std::to_string(x.first)+" is above bin "+std::to_string(x.second));
            //            x.second=nyq-1;
        }
        //        cout<<"Bins "<<x.first<<"->"<<x.second<<endl;
    }

    //For efficiency preallocate a complex matrix for performing computations:
    MatrixXcd complexWorkspace(nyq,9);

    //Store copies of sensor FFT values for window in these matrices:
    MatrixXcd fftAllSensors(nyq,9);

    //Nine columns of cross products table:
    MatrixXcd fftSensor1(nyq,9);
    MatrixXcd fftSensor2(nyq,8);
    MatrixXcd fftSensor3(nyq,7);
    MatrixXcd fftSensor4(nyq,6);
    MatrixXcd fftSensor5(nyq,5);
    MatrixXcd fftSensor6(nyq,4);
    MatrixXcd fftSensor7(nyq,3);
    MatrixXcd fftSensor8(nyq,2);
    MatrixXcd fftSensor9(nyq,1);

    cout<<"Processing stations:"<<endl;
    vector< complex<double> > allTables; //This will temporarily store all the data to be written to cross powers files for later use in creating transfer functions, along with GPS data.
    vector< complex<double> > DebugInfo; //Time Domain inner product between channels
    //Process window-by-window
    for(vector<uint64_t>::iterator i=indices.begin(); i!=indices.end();++i){
        //    for(vector<uint64_t>::iterator i=indices.begin(); i!=(indices.begin()+10);++i){
        if((i-indices.begin())%100==0){
            cout<<"Index "<<(i-indices.begin())<<" of "<<indices.size()<<endl;
        }
        //Precede table of cross powers with GPS record for this window
        Timestamps::Timestamp tValue = timestamps.getTimestampAtSample(*i);
        GPS::GPSRecord gpsValues = gps.getGPSatTime(tValue.timestamp-UTC_Offset);//This will change to access a gps location via a timestamp value.
        allTables.push_back(complex<double>(gpsValues.latitude,0.0));
        allTables.push_back(complex<double>(gpsValues.longitude,0.0));
        allTables.push_back(complex<double>(tValue.timestamp,0.0));
        //Perform FFT for new window in series.
        loadWorkspace(ch1,ch2,ch3,ch4,ch5,ch6,ch7,ch8,ch9,*i,workspace);
        //Add debug info 17 spaces total:
        // TDCrossPowersWindows(DebugInfo, workspace);
        // allTables.insert(allTables.end(),DebugInfo.begin(),DebugInfo.end());
        //Load a matrix for each window FFT:
        fftWindows(window,workspace,fftAllSensors);
        fftSensor1 = (fftAllSensors.col(0)*MatrixXcd::Ones(1,9)).conjugate();
        fftSensor2 = (fftAllSensors.col(1)*MatrixXcd::Ones(1,8)).conjugate();
        fftSensor3 = (fftAllSensors.col(2)*MatrixXcd::Ones(1,7)).conjugate();
        fftSensor4 = (fftAllSensors.col(3)*MatrixXcd::Ones(1,6)).conjugate();
        fftSensor5 = (fftAllSensors.col(4)*MatrixXcd::Ones(1,5)).conjugate();
        fftSensor6 = (fftAllSensors.col(5)*MatrixXcd::Ones(1,4)).conjugate();
        fftSensor7 = (fftAllSensors.col(6)*MatrixXcd::Ones(1,3)).conjugate();
        fftSensor8 = (fftAllSensors.col(7)*MatrixXcd::Ones(1,2)).conjugate();
        fftSensor9 = (fftAllSensors.col(8)*MatrixXcd::Ones(1,1)).conjugate();
        //Multiply matrices to produce table of cross powers (and autopowers) for every frequency bin
        fftSensor1 = fftSensor1.cwiseProduct(fftAllSensors.rightCols(9));
        fftSensor2 = fftSensor2.cwiseProduct(fftAllSensors.rightCols(8));
        fftSensor3 = fftSensor3.cwiseProduct(fftAllSensors.rightCols(7));
        fftSensor4 = fftSensor4.cwiseProduct(fftAllSensors.rightCols(6));
        fftSensor5 = fftSensor5.cwiseProduct(fftAllSensors.rightCols(5));
        fftSensor6 = fftSensor6.cwiseProduct(fftAllSensors.rightCols(4));
        fftSensor7 = fftSensor7.cwiseProduct(fftAllSensors.rightCols(3));
        fftSensor8 = fftSensor8.cwiseProduct(fftAllSensors.rightCols(2));
        fftSensor9 = fftSensor9.cwiseProduct(fftAllSensors.rightCols(1));
        //Run through frequency bands summing rows together and gathering terms for each sensor combination with use of Q-window:
        for(vector< pair< size_t,size_t> >::const_iterator j=bands->begin(); j!=bands->end();++j){
            //frequency band corresponds to top and bottom row of summation.
            pair< size_t,size_t> band = make_pair(j->first,j->second);
            sumCrossPowers(allTables,band,complexWorkspace,fftSensor1);
            sumCrossPowers(allTables,band,complexWorkspace,fftSensor2);
            sumCrossPowers(allTables,band,complexWorkspace,fftSensor3);
            sumCrossPowers(allTables,band,complexWorkspace,fftSensor4);
            sumCrossPowers(allTables,band,complexWorkspace,fftSensor5);
            sumCrossPowers(allTables,band,complexWorkspace,fftSensor6);
            sumCrossPowers(allTables,band,complexWorkspace,fftSensor7);
            sumCrossPowers(allTables,band,complexWorkspace,fftSensor8);
            sumCrossPowers(allTables,band,complexWorkspace,fftSensor9);
        }
    }
    //Adjust bands to reflect frequencies
    vector<pair<size_t,size_t> > freqBands;
    for(auto &band: *bands){
        size_t Start = band.first*(51200.0/(double)n);
        size_t Fin = band.second*(51200.0/(double)n);
        freqBands.push_back({Start,Fin});
    }
    //Write calculated values to binary files
    writeToBinaries(allTables,(9*10/2),outFileName,freqBands);
    cout<<"Done all"<<endl;
}

void XPower9coil::loadWorkspace(optional<ifstream> &ch1, optional<ifstream> &ch2, optional<ifstream> &ch3,optional<ifstream> &ch4,
                                optional<ifstream> &ch5, optional<ifstream> &ch6,optional<ifstream> &ch7,
                                optional<ifstream> &ch8, optional<ifstream> &ch9,const size_t index,vector< std::vector<double> > &workspace){
    ///Copy data into workspace.

    const size_t length = workspace.front().size(); //window length
    workspace[0].assign(length,0.0);
    workspace[1].assign(length,0.0);
    workspace[2].assign(length,0.0);
    workspace[3].assign(length,0.0);
    workspace[4].assign(length,0.0);
    workspace[5].assign(length,0.0);
    workspace[6].assign(length,0.0);
    workspace[7].assign(length,0.0);
    workspace[8].assign(length,0.0);
    workspace[9].assign(length,0.0); //Empty

    if(ch1){
        loadIntoVector(ch1, index, length,workspace[0]);
    }
    if(ch2){
        loadIntoVector(ch2, index, length,workspace[1]);
    }
    if(ch3){
        loadIntoVector(ch3, index, length,workspace[2]);
    }
    if(ch4){
        loadIntoVector(ch4, index, length,workspace[3]);
    }
    if(ch5){
        loadIntoVector(ch5, index, length,workspace[4]);
    }
    if(ch6){
        loadIntoVector(ch6, index, length,workspace[5]);
    }
    if(ch7){
        loadIntoVector(ch7, index, length,workspace[6]);
    }
    if(ch8){
        loadIntoVector(ch8, index, length,workspace[7]);
    }
    if(ch9){
        loadIntoVector(ch9, index, length,workspace[8]);
    }

    //    ...ch2,ch3,ch4...
}

void XPower9coil::fftWindows(const vector<double> &tsWindow, std::vector<std::vector<double> > &workspace, MatrixXcd &complexWorkspace)
{
    ///Find FFTs of 9 time series in workspace. Apply timeseries window function, and copy to complexWorkspace columns (1-9).
    ///TO DO: This can be done in parallel.

    //Error Check:
    for(auto i = workspace.begin();i!=workspace.end();++i){
        if(i->size() != tsWindow.size()){
            throw runtime_error("Size mismatch in_placeFFT: index: "+to_string(i-workspace.begin())+" "+to_string(i->size())+" : "+to_string(tsWindow.size()));
        }
    }

    r2r_FFT(workspace[0],workspace[9],tsWindow);
    r2r_FFT(workspace[1],workspace[9],tsWindow);
    r2r_FFT(workspace[2],workspace[9],tsWindow);
    r2r_FFT(workspace[3],workspace[9],tsWindow);
    r2r_FFT(workspace[4],workspace[9],tsWindow);
    r2r_FFT(workspace[5],workspace[9],tsWindow);
    r2r_FFT(workspace[6],workspace[9],tsWindow);
    r2r_FFT(workspace[7],workspace[9],tsWindow);
    r2r_FFT(workspace[8],workspace[9],tsWindow);

    //Transfer FFT from vector to complexWorkspace
    swtichToEigen(workspace,complexWorkspace);
}

void XPower9coil::TDCrossPowersWindows(std::vector<std::complex< double > > &TDTable, const std::vector<std::vector<double>> &workspace) {
    ///Produce inner product of each of the time series.

    const size_t num_vectors = workspace.size() - 1; //Last column of workspace is empty
    if (num_vectors < 2) {
        throw std::invalid_argument("Workspace must contain at least two vectors.");
    }

    TDTable.assign(17, (0,0)); //17 spaces for debug info
    //Inner-product of ch1 with itself,
    double result = std::inner_product(workspace[0].begin(), workspace[0].end(), workspace[0].begin(), 0.0);
    TDTable[0] = std::complex< double >(result,0.0);
    // and ch2 with itself
    double result2 = std::inner_product(workspace[1].begin(), workspace[1].end(), workspace[1].begin(), 0.0);
    // Push the result to TDTable
    TDTable[1] = std::complex< double >(result2,0.0);
    TDTable[2] = std::complex< double >((180.0/3.14)*std::atan2l(result,result2), 0.0);
}

void XPower9coil::r2r_FFT(vector<double> &coilData, std::vector<double> &copy, const vector<double> &tsWindow){
    ///FFT routine. Use "copy" as a workspace for performing the FFT and place result in "coilData"
    /// Imaginary numbers are stored on the opposite side of the array from their Real counterparts.

    //Error Check:
    if(coilData.size() != tsWindow.size()){
        throw runtime_error("Size mismatch in_placeFFT  coilData: "+to_string(coilData.size())+" vs  tsWindow: "+to_string(tsWindow.size()));
    }
    if(coilData.size() != copy.size()){
        throw runtime_error("Size mismatch in_placeFFT coilData: "+to_string(coilData.size())+" vs  copy: "+to_string(copy.size()));
    }

    //Prepare FFT
    fftw_plan p;
    int N = (int)coilData.size();
    p =  fftw_plan_r2r_1d(N, &copy[0], &copy[0], FFTW_R2HC,0);
    //Apply window
    for(size_t i=0;i<tsWindow.size();i++){
        copy[i] = coilData[i]*tsWindow[i];
    }
    fftw_execute(p);
    fftw_destroy_plan(p);
    coilData.assign(copy.begin(),copy.end());
}

void XPower9coil::swtichToEigen(std::vector<std::vector<double> > &workspace, MatrixXcd &complexWorkspace)
{///Load values from vectors to Eigen matrix:
    //Error check:
    if(complexWorkspace.rows()<=0){ //This is just here to make sure the conversion to uint64_t is correct.
        throw runtime_error("Malfunction in Eigen :"+to_string(complexWorkspace.rows()<=0));
    }
    if(calculateNyquistBin(workspace.front().size()) != (uint64_t)complexWorkspace.rows()){
        throw runtime_error("switchToEigen :"+to_string(workspace.front().size())+" vs "+to_string(complexWorkspace.rows()));
    }
    if(workspace.front().size() %2 ==0){
        //Series length affects the placement of the bins. Even:
        for(int i=0;i<9;i++){
            complexWorkspace(0,i)=workspace[i][0]; //DC
            complexWorkspace(complexWorkspace.rows()-1,i)=workspace[i][complexWorkspace.rows()-1];
            size_t m=workspace.front().size()-1;
            for(int j=1;j<(complexWorkspace.rows()-1);j++){
                complexWorkspace(j,i)=complex<double>(workspace[i][j],workspace[i][m]);
                m--;
            }
        }
    }
    else{
        //Series length affects the placement of the bins. Odd:
        for(int i=0;i<9;i++){
            complexWorkspace(0,i)=workspace[i][0]; //DC
            size_t m=workspace.front().size()-1;
            for(int j=1;j<=complexWorkspace.rows();j++){
                complexWorkspace(j,i)=complex<double>(workspace[i][j],workspace[i][m]);
                m--;
            }
        }
    }
}

void XPower9coil::writeToBinaries(const std::vector<std::complex<double> > &allTables,const size_t tableSize, const string outFileName, const std::vector<std::pair<size_t, size_t> > &bands)
{
    ///Extract the crosspowers and gps data for each frequency band from allTables and write to a binary file named according to the frequency.
    for(vector<pair<size_t, size_t> >::const_iterator i=bands.begin();i!=bands.end();++i){
        //Append frequency band to name
        std::pair<size_t, size_t> band = *i;
        //        const size_t tableSize=((7*6)/2); //6 sensors, cross powers and auto powers.
        // const size_t stnSpace=3+17+tableSize*bands.size();
        // const size_t bandOffset=3+17+tableSize*(i-bands.begin()); //position of band table cross powers from beginning of station.
        const size_t stnSpace=3+tableSize*bands.size();
        const size_t bandOffset=3+tableSize*(i-bands.begin()); //position of band table cross powers from beginning of station.
        const string outFreqFileName = outFileName+"_"+to_string(band.first)+"_"+to_string(band.second)+".bin";
        cout<<std::filesystem::path(outFreqFileName)<<endl;
        //Open binary output file for cross powers, lat, long, UTC time for each station
        ofstream outFile;
        outFile.open(outFreqFileName.c_str(),ios::out | ios::binary);
        if(!(outFile.is_open())){
            throw runtime_error("Couldn't open binary file: "+outFreqFileName);
        }
        //
        for(vector< complex<double> >::const_iterator stn=allTables.begin();stn!=allTables.end();stn=stn+stnSpace){
            vector< complex<double> >::const_iterator withinStn=stn;
            //Write debug information
            double lat = withinStn->real();
            double longitude = (withinStn+1)->real();
            double ts = (double)((withinStn+2)->real());
            outFile.write((char *) &lat, sizeof(double));
            outFile.write((char *) &longitude, sizeof(double));
            outFile.write((char *) &ts, sizeof(double));
            // for(size_t i=0; i<17;i++){ //17 spaces for additional debug info i.e. Ch1 amplitude, Ch2 amplitude, polarization angle
            //     double realPart = (withinStn+i)->real();
            //     double imagPart = (withinStn+i)->imag();
            //     outFile.write((char *) &realPart, sizeof(double));
            //     outFile.write((char *) &imagPart, sizeof(double));
            // }
            //outFile<<lat<<longitude<<ts;
            for(size_t i=0; i<(tableSize);i++){
                double realPart = (withinStn+bandOffset+i)->real();
                double imagPart = (withinStn+bandOffset+i)->imag();
                outFile.write((char *) &realPart, sizeof(double));
                outFile.write((char *) &imagPart, sizeof(double));
                //outFile<<*(withinStn+bandOffset+i); //cross power values.
            }
        }
        outFile.close();
        cout<<"Done writing table to binary file: "<<outFreqFileName<<endl;
    }
}

void XPower9coil::writeToBinaries(const std::vector<double> &allTables, const std::string &outFileName) {
    std::ofstream outFile(outFileName+".bin", std::ios::binary);
    if (!outFile) {
        throw std::runtime_error("Could not open file for writing");
    }

    // Write the size of the vector first (optional)
    size_t size = allTables.size();

    // Write the data
    outFile.write(reinterpret_cast<const char*>(allTables.data()), size * sizeof(double));

    // Close the file
    outFile.close();
}

std::vector<uint64_t> XPower9coil::getSfericList(const size_t tsSampleLength, const size_t windowLength, const std::optional<std::string> indicesFile, std::optional<std::filesystem::path> toINIFILE)
{
    //Check that window length is correct size for the time series. Determine correct indices.
    std::vector< uint64_t > indicesList;
    optional<ifstream> ch = openFile(indicesFile, toINIFILE);
    if (!ch) {
        cout<<"Missing sfericsListFile, using default spacing of "<<to_string(windowLength/2)<<endl;
        const size_t L = (tsSampleLength - (tsSampleLength%windowLength))/(windowLength/2);
        for(size_t i=0; i<L; i++){
            indicesList.push_back((windowLength/2)*i);
        }
    } else {
        ch->seekg(0, ios_base::end);
        const uint64_t indicesFileLength = ch->tellg();
        ch->seekg(0, ios_base::beg);

        //Error Check:
        if(indicesFileLength % 8 != 0){
            throw runtime_error("sfericIndices wrong size to be filled with uint64 values: "+to_string(indicesFileLength)+"bytes");
        }
        if(!ch->good()) {
            throw runtime_error("Error reading SfericsList file");
        }

        //Load index points into vector
        uint64_t strt;
        for(uint64_t i = 0; i < (indicesFileLength/8); i++){
            ch->read((char *) &strt, sizeof(uint64_t));
            indicesList.push_back(strt);
        }
        ch->close();

        //Error Check:
        if(!is_sorted(indicesList.begin(),indicesList.end())){
            throw runtime_error("sfericIndices are not sorted from small to big. Check for errors in file.");
        }
        if((indicesList.back()+windowLength) > tsSampleLength){
            throw runtime_error("last index:"+to_string(indicesList.back())+" exceeds time series length: "+to_string(tsSampleLength)+" given the window length chosen: "+to_string(windowLength));
        }
        if(indicesList.front() < windowLength/2){
            throw runtime_error("last index:"+to_string(indicesList.front())+" is less than window length/2: "+to_string(windowLength/2));
        }
        //Add a space to front so that window peak and sferic peak are aligned:
        for(auto it = indicesList.begin(); it != indicesList.end(); ++it){
            *it = *it - (windowLength/2);
        }
    }
    return indicesList;
}

INIValues XPower9coil::readIniFile(std::string iniFileName)
{    
    //Error Check
    std::filesystem::path f(iniFileName);
    if (!exists(f)){
        throw runtime_error("Cannot open ini file: "+iniFileName);
    }

    INIValues LocalValues;

    EmProIniFile INIREADER = EmProIniFile(iniFileName);
    if(!INIREADER.getInt("XPowers9Coil","length")){
        cout<<"Couldn't find XPowers9Coil.length "<<endl;
    }
    LocalValues.length = INIREADER.getInt("XPowers9Coil","length").value();

    if(!(INIREADER.getString("XPowers9Coil","x") && INIREADER.getString("XPowers9Coil","y") && INIREADER.getString("XPowers9Coil","z"))){
        cout<<"INIFILE needs as least entries for x,y,z "<<endl;
    }
    LocalValues.x = INIREADER.getString("XPowers9Coil","x").value();
    LocalValues.y = INIREADER.getString("XPowers9Coil","y").value();
    LocalValues.z = INIREADER.getString("XPowers9Coil","z").value();

    if(!INIREADER.getString("XPowers9Coil","windowType")){
        cout<<"Couldn't find XPowers9Coil.windowType "<<endl;
    }
    LocalValues.windowType = INIREADER.getString("XPowers9Coil","windowType").value();

    if(!INIREADER.getString("XPowers9Coil","GPSLog")){
        cout<<"Couldn't find XPowers9Coil.GPSLog "<<endl;
    }
    LocalValues.GPSLog = INIREADER.getString("XPowers9Coil","GPSLog").value();

    if(!INIREADER.getString("XPowers9Coil","outFileName")){
        cout<<"Couldn't find XPowers9Coil.outFileName "<<endl;
    }
    LocalValues.outFileName = INIREADER.getString("XPowers9Coil","outFileName").value();

    if(!INIREADER.getString("XPowers9Coil","bands")){
        cout<<"Couldn't find XPowers9Coil.bands "<<endl;
    }
    LocalValues.bands = INIREADER.getString("XPowers9Coil","bands").value();

    if(auto SLF = INIREADER.getString("XPowers9Coil","sfericsListFile")){
        cout<<"Using sfericsList: "<<*SLF<<endl;
        LocalValues.sfericsListFile=SLF;
    }
    else{
        cout<<"Couldn't find XPowers9Coil.sfericsListFile "<<endl;
        LocalValues.sfericsListFile=nullopt;
    }
    if(!INIREADER.getString("XPowers9Coil","Timestamps")){
        cout<<"Couldn't find XPowers9Coil.Timestamps "<<endl;
    }
    LocalValues.Timestamps = INIREADER.getString("XPowers9Coil","Timestamps").value();

    //INIFILE FORMAT:
    //#Cross Powers Section:
    //[XPowers9Coil]
    //x = C:/Users/johng/Desktop/NVBX22_06_16_16_04_51/RD4TF/channel1Filt.bin
    //y = C:/Users/johng/Desktop/NVBX22_06_16_16_04_51/RD4TF/channel2Filt.bin
    //z = C:/Users/johng/Desktop/NVBX22_06_16_16_04_51/RD4TF/channel3Filt.bin
    //#xb = C:/NVBX22_06_16_16_10_33/RD4TF/channel1Filt.bin
    //#yb = C:/NVBX22_06_16_16_10_33/RD4TF/channel2Filt.bin
    //#zb = C:/NVBX22_06_16_16_10_33/RD4TF/channel3Filt.bin
    //#xr = C:/NVBX22_06_13_17_13_15/RD4TF/channel1Filt.bin
    //#yr = C:/NVBX22_06_13_17_13_15/RD4TF/channel2Filt.bin
    //#zr = C:/NVBX22_06_13_17_13_15/RD4TF/channel3Filt.bin
    //#sfericsListFile  = C:/Users/johng/Desktop/01Jun2019_1405/rd4TF/sfericIndices.bin
    //length = 50000
    //windowType = harris
    //sampleRate = 50000
    //Timestamps = C:/Users/johng/Desktop/NVBX22_06_16_16_04_51/timestamps.bin
    //GPSLog = C:/Users/johng/Desktop/NVBX22_06_16_16_04_51/LatLong.bin
    //fileName = C:/Users/johng/Desktop/NVBX22_06_16_16_04_51/RD4TF/crosspowers.bin
    //bands = 25,38,50,60,60,70,25,55,70,300,4500,7000
    //---------------------------------------
    //          SPECIAL OPTIONS:
    //---------------------------------------
    //CreateFakeTimestampsAndGPS = 1 (optional) Overrides normal functioning of
    //program to create a fake Timestamps and GPS file consistent with the length
    //of the data series

    return LocalValues;
}

XPower9coil::XPower9coil(const std::string iniFileName)
{
    //Use Boost to read info from iniFileName.
    iniFile = readIniFile(iniFileName);
}

void XPower9coil::sumCrossPowers(vector< complex<double> > &allTables, pair< size_t,size_t> &band, MatrixXcd &complexWorkspace,const MatrixXcd &crossPowerMatrix){
    ///Compute table of cross powers for frequency band summing to produce complex numbers for each coil pair in the crossPowerMatrix. Save in allTables.
    const size_t lowerFreq=band.first;
    const size_t upperFreq=band.second;
    const size_t columns = crossPowerMatrix.cols();
    //write Q-window to columns:
    applyQwindow(complexWorkspace,(upperFreq-lowerFreq+1));
    //Apply Q-window to data, multiply by conjugates, sum along columns to produce cross power summations:
    complexWorkspace.block(0,0,(upperFreq-lowerFreq+1),columns) = complexWorkspace.block(0,0,(upperFreq-lowerFreq+1),columns).cwiseProduct(crossPowerMatrix.middleRows(lowerFreq,(upperFreq-lowerFreq+1)));
    MatrixXcd result=complexWorkspace.block(0,0,(upperFreq-lowerFreq+1),columns).colwise().sum();
    //MatrixXcd result=complexWorkspace.block(0,0,(upperFreq-lowerFreq+1),columns).cwiseProduct(crossPowerMatrix.middleRows(lowerFreq,(upperFreq-lowerFreq+1))).colwise().sum();
    //Write to binary file
    for (int64_t i=0;i<result.cols();i++){
        allTables.push_back(result(i));
    }
}

void XPower9coil::applyQwindow(MatrixXcd &complexWorkspace, const size_t numberOfRows){
    ///create a Q window "numberOfRows" long and fill all 9 columns of complexWorkspace.
    complexWorkspace.setZero();//Clear the matrix
    for(size_t i = 0;i<numberOfRows;i++){
        complexWorkspace(i,0)=sin((3.141592653589*((double)i))/((double)(numberOfRows-1))); //half a wavelength
    }
    complexWorkspace.block(0,0,numberOfRows,9) = complexWorkspace.block(0,0,numberOfRows,1)*(MatrixXcd::Ones(1,9));//Copies column0 to columns0-9
}

XPower9coil::GPS XPower9coil::getGPSRecords(const std::string nodeName, std::optional<std::filesystem::path> toINIFILE)
{
    ///Open file containing GPS records as specified in INI file. Return GPS class with data.
    GPS temp;
    optional<ifstream> in = openFile(nodeName,toINIFILE);
    if(!in) throw runtime_error("Problem opening "+nodeName);
    temp.loadRecordsFromFile(*in);
    // Depending on how the GPS records are stored, this could either be a
    // binary or a text file listing the positions on the survey course.
    return temp;
}

XPower9coil::Timestamps XPower9coil::getTimestampRecords(optional<std::string> filename, std::optional<std::filesystem::path> toINIFILE)
{
    ///Open file containing GPS records as specified in INI file. Return GPS class with data.
    if(!filename){
        throw std::runtime_error("Error finding filename in getTimestampRecords");
    }
    Timestamps temp;
    optional<ifstream> in = openFile(filename,toINIFILE);
    if(!in) throw runtime_error("Problem opening "+(*filename));
    temp.loadRecordsFromFile(*in);
    //
    return temp;
}

double XPower9coil::calculateShiftUTC(GPS gps, Timestamps timestamps)
{
    ///Get GPS and local time records and do comparison
    vector<double> gpsTS;
    {
        vector<uint32_t> temp = gps.getTimestamps();
        for(auto &x:temp){
            gpsTS.push_back(static_cast<double>(x)); //extract gps fix times
        }
    }
    vector<double> tsPPS = timestamps.getTimeValues();
    double diff = (tsPPS.front()-gpsTS.front()); //TO DO: Get fancy and do a least squares solve throwing exception if misfit is high.
    diff -= remainder(diff,3600.0);
    return diff;
}
