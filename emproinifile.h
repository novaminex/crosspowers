#ifndef EMPROINIFILE_H
#define EMPROINIFILE_H

#include <vector>
#include <string>
#include <optional>

//Ben Polzer Feb 2025
//Lightweight ini file reader/writer with no dependencies other than C++17

//Features:
//Provides get/put access methods for variables of string,double,int,bool,
//   std::vector<string>,std::vector<double>,std::vector<int>, std::vector<bool>
//  section and variable names are case independent
//preserves comments and formatting when saving the file


//Additional info
//For scalar objects uses std::optional in case the variable cant be found
//Methods returning vector objects will return an empty vector if the name/variable could not be found
//One or more error strings can be recovered using the getLastErrorString() and getErrorStrings() methods

//example usage
//
//  EmProIniFile  iniReadWrite(existingIniFilePath);
//  std::vector<double> coilCals=iniReadWrite.getVectorDouble("StackingParams","CoilCal");
//  if(coilCals.size()==0){
//  possible error
//  MyLogger::log(iniReadWrite.getErrorString());
//  return false;
//  }
//  coilCals[1]=1.53;//change one
//  iniReadWrite.putVectorDouble("StackingParams","CoilCal",coilCals, 3);//write it back to class state
//  iniReadWrite.saveFile() ;  //save it to disk file
//

class EmProIniFile
{

    struct iniFileLineRecord{
        std::string lineOfText;  //raw line from the file
        std::string sectionName;//zero length if doesnt contain a section name
        std::string variableName;//zero length if doesnt contain a variable name
        std::string value;//zero length if doesnt contain a value
    };

private:
    std::string m_filePath;
    std::vector<iniFileLineRecord> m_inifileCache;
    std::vector<std::string>m_errorStrings;

    std::optional<std::string> getVariableOptional(const std::string &Section,const std::string &Variable);
    void addToIniCache(const std::string &textLine, const std::string &sectionName, const std::string &variableName, const std::string &value );

    //simple utility routines
    void removeQuotes(std::string &s);
    std::string removeWhitespace(const std::string& str);
    std::string stringToLower(const std::string& s);
    void removeLeadingAndTrailingWhitespace( std::string& s);
    void local_stringChopper(const std::string& str, const std::string& delim, std::vector<std::string>& parts) ;
    std::string extractSectionName(const std::string s);


public:

    EmProIniFile(std::string filePath);

    std::optional<double>getDouble(const std::string &Section,const std::string &Variable);
    std::optional<int> getInt(const std::string &Section,const std::string &Variable);
    std::optional<bool> getBool(const std::string &Section,const std::string &Variable);

    std::optional<std::string> getString(const std::string &Section,const std::string &Variable);
    std::optional<std::string> getString(const std::string &Section,
                                         const std::string &Variable,
                                         const std::string &defaultValueIfMissing);

    std::vector<double> getVectorDouble(const std::string &Section,const std::string &Variable);
    std::vector<bool> getVectorBool(const std::string &Section,const std::string &Variable);
    std::vector< int> getVectorInt(const std::string &Section,const std::string &Variable);
    std::vector<std::string> getVectorString(const std::string &Section,const std::string &Variable);

    void putString(const std::string &Section,const std::string &Variable,const std::string &stringToPut);
    void putDouble(const std::string &Section,const std::string &Variable, double doubleToPut, int ndecimals);
    void putInt(const std::string &Section,const std::string &Variable, int intToPut);
    void putBool(const std::string &Section,const std::string &Variable, bool boolToPut);
    void putVectorDouble(const std::string &Section,const std::string &Variable,const  std::vector<double> &doubleToPut, int ndecimals);
    void putVectorInt(const std::string &Section,const std::string &Variable,const  std::vector<int> &intToPut);
    void putVectorString(const std::string &Section,const std::string &Variable,const  std::vector<std::string> &stringToPut);

    bool entryExists(const std::string &Section,const std::string &Variable);
    bool entryExistsWordy(const std::string &Section,const std::string &Variable);
    bool sectionExists(const std::string sectionName);

    void saveToFileEntriesOnly(std::string path);
    void saveToFile(std::string path);
    void saveFile();

    std::string getLastErrorString();
    std::vector<std::string> getErrorStrings();

};


#endif // EMPROINIFILE_H
